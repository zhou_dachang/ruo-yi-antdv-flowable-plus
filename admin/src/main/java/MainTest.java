import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MainTest {

    public static void main(String[] args)throws IOException {

        String fileName="/Users/zhouxuan/bbb.xlsx";
        EasyExcel.write(fileName)
                //注册策略
                .registerWriteHandler(new CustomCellWriteHandler())
                .sheet("模板1")
                .doWrite(new ArrayList<>());
//        File file = new File("/Users/zhouxuan/Desktop/资金支付申请表.xlsx");
//        InputStream stream = new FileInputStream(file);
//        // 把excel流给这个对象，后续可以操作
//        XSSFWorkbook workbook = new XSSFWorkbook(stream);
//        // 设置模板的第一个sheet的名称，名称我们使用合同号
//        workbook.setSheetName(0, "测试sheet0");
//
//        // 把workbook写到流里
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        workbook.write(baos);
//        byte[] bytes = baos.toByteArray();
//        stream = new ByteArrayInputStream(bytes);
//        String out = "/Users/zhouxuan/aaa.xlsx";
//        OutputStream outputStream = new FileOutputStream(out);
//        ExcelWriter excelWriter = EasyExcel.write(outputStream)
//                .withTemplate(stream)
//                .registerWriteHandler(new CustomCellWriteHandler())
//                .build();
//        WriteSheet writeSheet = EasyExcel.writerSheet("测试sheet1")
//                .registerWriteHandler(new CustomCellWriteHandler()).build();
////        excelWriter.fill(new PlatformCrDto(), writeSheet);
//        excelWriter.write(new ArrayList<>(), writeSheet);
//        excelWriter.finish();
//        baos.close();
//        stream.close();

    }

}

class CustomCellWriteHandler implements CellWriteHandler {

    @Override
    public void afterCellDispose(CellWriteHandlerContext context) {
        System.out.println(1);
    }

    @Override
    public void beforeCellCreate(CellWriteHandlerContext context) {
        System.out.println(2);
    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
        System.out.println(3);
    }

    @Override
    public void afterCellCreate(CellWriteHandlerContext context) {
        System.out.println("cell = " + context);
        Cell cell = context.getCell();
        if (cell != null) {
            System.out.println(cell);
            // 这里可以对cell进行任何操作
            Sheet sheet = context.getWriteSheetHolder().getSheet();
            XSSFDrawing drawingPatriarch = (XSSFDrawing) sheet.createDrawingPatriarch();

            ClientAnchor anchor = drawingPatriarch.createAnchor(0, 0, 1023, 255, (short) 0, 0, (short) 0, 0);
            anchor.setRow1(12);
            anchor.setCol1(0);
            anchor.setRow2(13);
            anchor.setCol2(1);
            XSSFSimpleShape simpleShape = drawingPatriarch.createSimpleShape((XSSFClientAnchor) anchor);
            simpleShape.setShapeType(ShapeTypes.LINE);
            // 设置线宽
            simpleShape.setLineWidth(0.5);
            // 设置线的风格
            simpleShape.setLineStyle(0);
            // 设置线的颜色
            simpleShape.setLineStyleColor(0, 0, 0);
        }

    }

}
@Data
class PlatformCrDto implements java.io.Serializable {
    private static final long serialVersionUID = 5454155825314635342L;

    // 单据编号，作为sheet页名
    private String billNo;

    // 银行
    private String bank;

    // 银行银联号
    private String bankAccNo;

    // 银行账号
    private String bankNumber;

    // 公司名称
    private String branName;

    // 报销内容
    private String budgetName;

    // 合同金额
    private Double contractAmt;

    // 日期
    private String date;

    // 部门名称
    private String deptName;

    // 附件张数
    private String printCount;

    // 二进制的二维码
    private String printQrCode;

    // 工程名称
    private String projectSubName;

    // 收款人
    private String purName;

    // NC采购单号
    private String purcaserNcNo;

    // 审定金额
    private Double settlementAmt;

    // 实付金额
    private Double stlAmt;

    // 累计已支付金额
    private Double stlTotalAmt;

    // 其他扣款
    private Double otherAmt;

    // 质保金
    private Double warrantyAmt;

    // 金额大写
    private String capitalization;

    // 二维码图片
    private WriteCellData<Void> voidWriteCellData;



}
