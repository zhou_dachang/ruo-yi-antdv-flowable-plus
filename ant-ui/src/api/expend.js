import request from '@/utils/request'

// 查询财务管理列表
export function listTExpend (query) {
  return request({
    url: '/expend/list',
    method: 'get',
    params: query
  })
}

// 查询财务管理详细
export function getTExpend (id) {
  return request({
    url: '/expend/' + id,
    method: 'get'
  })
}

// 新增财务管理
export function addTExpend (data) {
  return request({
    url: '/expend',
    method: 'post',
    data: data
  })
}

// 修改财务管理
export function updateTExpend (data) {
  return request({
    url: '/expend',
    method: 'put',
    data: data
  })
}

// 删除财务管理
export function delTExpend (id) {
  return request({
    url: '/expend/' + id,
    method: 'delete'
  })
}

// 导出财务管理
export function exportTExpend (query) {
  return request({
    url: '/expend/export',
    method: 'get',
    params: query
  })
}
