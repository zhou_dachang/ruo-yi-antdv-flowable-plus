import request from '@/utils/request'

// 查询项目资金情况列表
export function listTProject (query) {
  return request({
    url: '/project/list',
    method: 'get',
    params: query
  })
}

// 查询项目资金情况详细
export function getTProject (id) {
  return request({
    url: '/project/' + id,
    method: 'get'
  })
}

// 新增项目资金情况
export function addTProject (data) {
  return request({
    url: '/project',
    method: 'post',
    data: data
  })
}

// 修改项目资金情况
export function updateTProject (data) {
  return request({
    url: '/project',
    method: 'put',
    data: data
  })
}

// 删除项目资金情况
export function delTProject (id) {
  return request({
    url: '/project/' + id,
    method: 'delete'
  })
}

// 导出项目资金情况
export function exportTProject (query) {
  return request({
    url: '/project/export',
    method: 'get',
    params: query
  })
}
