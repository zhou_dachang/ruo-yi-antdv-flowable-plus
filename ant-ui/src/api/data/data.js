import request from '@/utils/request'

export function getPathFiles (query) {
    return request({
        url: '/data/currentPathFiles',
        method: 'get',
        params: query
    })
}

export function createFolder(params) {
    return request({
        url: '/data/createFolder',
        method: 'put',
        params: params
    })
}

export function renameFolder(oldName,newName) {
    return request({
        url: '/data/renameFolder',
        method: 'post',
        params: {
            originName: oldName,
            newName: newName
        }
    })
}

export function renameFile(oldFile,newName) {
    return request({
        url: '/data/renameFile',
        method: 'post',
        params: {
            filePath: oldFile,
            newName: newName
        }
    })
}

export function deleteFile(path, name) {
    return request({
        url: '/data/deleteFile',
        method: 'delete',
        params: {
            filePath: path,
            fileName: name
        }
    })
}

export function deleteFolder(path){
    return request({
        url: '/data/deleteFolder',
        method: 'delete',
        params: {
            folderPath: path
        }
    })
}

export function uploadFile(formData) {
    return request({
        url: '/data/deleteFolder',
        method: 'delete',
        data:formData
    })
}
