import request from '@/utils/request'

// 查询安全检查类项目列表
export function listTProject (query) {
  return request({
    url: '/project/list',
    method: 'get',
    params: query
  })
}

// 查询安全检查类项目详细
export function getTProject (id) {
  return request({
    url: '/project/' + id,
    method: 'get'
  })
}

// 新增安全检查类项目
export function addTProject (data) {
  return request({
    url: '/project',
    method: 'post',
    data: data
  })
}

// 修改安全检查类项目
export function updateTProject (data) {
  return request({
    url: '/project',
    method: 'put',
    data: data
  })
}

// 删除安全检查类项目
export function delTProject (id) {
  return request({
    url: '/project/' + id,
    method: 'delete'
  })
}

// 导出安全检查类项目
export function exportTProject (query) {
  return request({
    url: '/project/tProject/export',
    method: 'get',
    params: query
  })
}
