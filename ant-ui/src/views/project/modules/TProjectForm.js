import AntModal from '@/components/pt/dialog/AntModal'
import { getTProject, addTProject, updateTProject } from '@/api/project/tProject'
import storage from 'store'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import FileUpload from '@/components/pt/uploader'

export default {
  name: 'CreateForm',
  props: {
    categoryOptions: {
      type: Array,
      required: true
    }},
  components: {
    AntModal,
    FileUpload
 },
  data () {
    return {
      open: false,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      total: 0,
      id: undefined,
      formTitle: '添加项目',
      // 表单参数
      form: {},
      headers: {
        Authorization: 'Bearer ' + storage.get(ACCESS_TOKEN)
      },
      uploadUrl: process.env.VUE_APP_BASE_API + '/common/upload',
      fileList: [],
      attachmentRefName: 'addUploaderFile', // 标志表单是否含有附件
      formId: '',
      rules: {
        projectName: [{ required: true, message: '项目名不能为空', trigger: 'blur' }],
        category: [{ required: true, message: '项目分类不能为空', trigger: 'blur' }],
        beginDate: [{ required: true, message: '起始时间不能为空', trigger: 'blur' }],
        finishDate: [{ required: true, message: '结束时间不能为空', trigger: 'blur' }]
      }
    }
  },
  filters: {},
  created () { 
    this.reset() 
  },
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        projectName: undefined,
        category: undefined,
        beginDate: undefined,
        finishDate: undefined,
        manager: undefined,
        location: undefined,
        schedule: undefined,
        contractAmount: undefined,
        advancePayment: undefined,
        paidAdvancePayment: undefined,
        balancePayment: undefined,
        paidBalancePayment: undefined,
        businessExpenses: undefined,
        programExpense: undefined,
        customerContact: undefined,
        customerContactInfo: undefined,
        safetyCheck: {
          id: undefined,
          projectId: undefined,
          checkAmount: undefined,
          finishedCheckAmount: undefined,
          reexamineAmount: undefined
        },
        safetyAssistance: {
          id: undefined,
          projectId: undefined,
          frequency: undefined,
          finishedCount: undefined,
          assistanceRecords: []
        },
        notes: undefined
      }
      if (this.formId) {
          // 清空附件的formId
          this.formId = ''
      }
    },
    handleUploadFile(info){
      this.fileList = info.fileList
      if (info.file.status === 'done') {
        const target = this.fileList[this.fileList.length-1]
        target.url = info.file.response.url
        target.fileName = info.file.response.fileName
        // this.fileList.push({
        //   uid: info.file.uid,
        //   name: info.file.name,
        //   url: info.file.response.url
        // })
      }
    },
    /** 新增按钮操作 */
    handleAdd () {
      this.reset()
      this.open = true
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const tProjectId = row.id
      getTProject(tProjectId).then(response => {
        this.form = response.data
        if(response.data.safetyAssistance.assistanceRecords.length > 0){
          response.data.safetyAssistance.assistanceRecords.forEach(i => {
            this.fileList.push({
              uid: i.id,
              name: i.name,
              status: 'done',
              url: i.url,
              fileName: i.relateFile
            })
          })
        }
        this.formId = response.data.id
        this.formTitle = '修改项目'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function (category) {
      if (category && (typeof category === 'number')) {
        this.form.category = category
      }
      // 附件转化fileList
      this.form.safetyAssistance.assistanceRecords = []
      this.fileList.forEach(element => {
        this.form.safetyAssistance.assistanceRecords.push({
          projectId: this.form.id,
          safetyAssistanceId: this.form.safetyAssistance.id,
          name: element.name,
          relateFile: element.fileName,
        })
      })
      this.$refs.form.validate(valid => {
        if (valid) {
            const saveForm = JSON.parse(JSON.stringify(this.form))
            if (this.form.id !== undefined) {
              updateTProject(saveForm).then(response => {
                this.$message.success('修改成功', 3)
                  this.open = false
                  this.$emit('ok')
                  this.$emit('close')
              })
            } else {
              addTProject(saveForm).then(response => {
                this.$message.success('新增成功', 3)
                this.open = false
                this.$emit('ok')
                this.$emit('close')
              })
            }
        } else {
          return false
        }
      })
    },
    back () {
      const index = '/project/check/index'
      this.$router.push(index)
    },
    uploadCompleteFile: function (successFile, errorFile) {
        if (errorFile.length > 0) {
            // 有附件保存失败的处理
            this.attachmentUploadStatus = false // 记录附件上传失败
            this.$refs[this.attachmentRefName].$el.scrollIntoView() // 页面滚动到附件位置
            this.uploaderButtonStatus = false // 按钮关闭loading状态
        } else {
            // 所有附件都保存成功的处理
            this.attachmentUploadStatus = true // 记录附件上传成功
            this.$message.success('保存成功', 3)
            this.open = false
            this.$emit('ok')
            this.$emit('close')
        }
        this.uploaderButtonStatus = false // 返回按钮可用
    }
  }
}
