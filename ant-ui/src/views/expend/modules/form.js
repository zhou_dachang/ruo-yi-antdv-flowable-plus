import AntModal from '@/components/pt/dialog/AntModal'
import { getTExpend, addTExpend, updateTExpend } from '@/api/expend'

export default {
  name: 'CreateForm',
  props: {
 },
  components: {
    AntModal },
  data () {
    return {
      open: false,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      total: 0,
      id: undefined,
      formTitle: '添加财务管理',
      // 表单参数
      form: {},
      rules: {
        name: [{ required: true, message: '支出名目不能为空', trigger: 'blur' }]
      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.open = true
      this.form = {
        id: undefined,
        name: undefined,
        expenses: undefined,
        notes: undefined
      }
    },
    /** 新增按钮操作 */
    handleAdd () {
      this.reset()
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const tExpendId = row.id
      getTExpend(tExpendId).then(response => {
        this.form = response.data
        this.formTitle = '修改财务管理'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function () {
      this.$refs.form.validate(valid => {
        if (valid) {
            const saveForm = JSON.parse(JSON.stringify(this.form))
			if (this.form.id !== undefined) {
				updateTExpend(saveForm).then(response => {
					this.$message.success('新增成功', 3)
					this.open = false
					this.$emit('ok')
					this.$emit('close')
				})
              } else {
				addTExpend(saveForm).then(response => {
					this.$message.success('新增成功', 3)
					this.open = false
					this.$emit('ok')
					this.$emit('close')
				})
			}
        } else {
          return false
        }
      })
    },
    back () {
      const index = '/project/texpend/index'
      this.$router.push(index)
    }
  }
}