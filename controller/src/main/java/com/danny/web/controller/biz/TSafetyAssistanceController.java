package com.danny.web.controller.biz;
import cn.hutool.core.util.StrUtil;
import java.util.List;
import com.danny.common.annotation.Log;
import com.danny.common.core.domain.R;
import com.danny.system.domain.TSafetyAssistance;
import com.danny.system.service.TSafetyAssistanceService;
import com.github.pagehelper.PageInfo;
import com.danny.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.danny.common.enums.BusinessType;
import com.danny.common.utils.poi.ExcelUtil;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.danny.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
@RestController
@RequestMapping("/project/tSafetyAssistance")
public class TSafetyAssistanceController extends BaseController{

    @Autowired
    private TSafetyAssistanceService tSafetyAssistanceService;

    /**
     * 查询安全帮扶列表
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:list')")
    @GetMapping("/list")
    public R<Page> list(TSafetyAssistance param, HttpServletRequest request, HttpServletResponse response)
    {
        LambdaQueryWrapper<TSafetyAssistance> query = Wrappers.<TSafetyAssistance>lambdaQuery();

        query.eq(StrUtil.isNotBlank(param.getProjectId()),TSafetyAssistance::getProjectId,param.getProjectId());
        query.eq(param.getFrequency() != null,TSafetyAssistance::getFrequency,param.getFrequency());

        PageDomain pageDomain = new PageDomain(request, response);
        Page page = tSafetyAssistanceService.page(new Page(pageDomain.getPageNum(), pageDomain.getPageSize()), query);
        return R.data(page);
    }

    /**
     * 获取安全帮扶详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:query')")
    @GetMapping(value = "/{id}")
    public R<TSafetyAssistance> detail(@PathVariable("id") String id)
    {
        return R.data(tSafetyAssistanceService.getById(id));
    }

    /**
     * 新增安全帮扶
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:add')")
    @Log(title = "安全帮扶", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  TSafetyAssistance tSafetyAssistance)
    {
        return R.status(tSafetyAssistanceService.saveOrUpdate(tSafetyAssistance));
    }

    /**
     * 修改安全帮扶
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:edit')")
    @Log(title = "安全帮扶", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated TSafetyAssistance tSafetyAssistance)
    {
        return R.status(tSafetyAssistanceService.saveOrUpdate(tSafetyAssistance));
    }


    /**
     * 删除安全帮扶
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:remove')")
    @Log(title = "安全帮扶", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R remove(@PathVariable String[] ids)
    {
        return R.status(tSafetyAssistanceService.deleteTSafetyAssistanceByIds(ids));
    }


    /**
     * 导出安全帮扶列表
     */
    @PreAuthorize("@ss.hasPermi('project:tSafetyAssistance:export')")
    @Log(title = "安全帮扶", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(TSafetyAssistance param)
    {


        LambdaQueryWrapper<TSafetyAssistance> query = Wrappers.<TSafetyAssistance>lambdaQuery();

        query.eq(StrUtil.isNotBlank(param.getProjectId()),TSafetyAssistance::getProjectId,param.getProjectId());
        query.eq(param.getFrequency() != null, TSafetyAssistance::getFrequency,param.getFrequency());

        List<TSafetyAssistance> list = tSafetyAssistanceService.list(query);
        ExcelUtil<TSafetyAssistance> util = new ExcelUtil<TSafetyAssistance>(TSafetyAssistance.class);
        return util.exportExcel(list, "安全帮扶数据");
    }
}
