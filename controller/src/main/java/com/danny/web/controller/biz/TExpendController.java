package com.danny.web.controller.biz;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.danny.common.core.controller.BaseController;
import com.danny.common.utils.poi.ExcelUtil;
import com.danny.system.domain.TExpend;
import com.danny.system.service.TExpendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.hutool.core.util.StrUtil;
import com.danny.common.annotation.Log;
import com.danny.common.core.domain.R;
import com.danny.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.danny.common.enums.BusinessType;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

@RestController
@RequestMapping("/expend")
public class TExpendController extends BaseController {

    @Autowired
    private TExpendService tExpendService;

    /**
     * 查询财务管理列表
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:list')")
    @GetMapping("/list")
    public R<Page> list(TExpend param, HttpServletRequest request, HttpServletResponse response)
    {
        LambdaQueryWrapper<TExpend> query = Wrappers.<TExpend>lambdaQuery();

        query.like(StrUtil.isNotBlank(param.getName()),TExpend::getName,param.getName());

        PageDomain pageDomain = new PageDomain(request, response);
        Page page = tExpendService.page(new Page(pageDomain.getPageNum(), pageDomain.getPageSize()), query);
        return R.data(page);
    }

    /**
     * 获取财务管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:query')")
    @GetMapping(value = "/{id}")
    public R<TExpend> detail(@PathVariable("id") String id)
    {
        return R.data(tExpendService.getById(id));
    }

    /**
     * 新增财务管理
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:add')")
    @Log(title = "财务管理", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  TExpend tExpend)
    {
        return R.status(tExpendService.saveOrUpdate(tExpend));
    }

    /**
     * 修改财务管理
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:edit')")
    @Log(title = "财务管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated TExpend tExpend)
    {
        return R.status(tExpendService.saveOrUpdate(tExpend));
    }


    /**
     * 删除财务管理
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:remove')")
    @Log(title = "财务管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R remove(@PathVariable String[] ids)
    {
        return R.status(tExpendService.deleteTExpendByIds(ids));
    }


    /**
     * 导出财务管理列表
     */
    @PreAuthorize("@ss.hasPermi('project:tExpend:export')")
    @Log(title = "财务管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(TExpend param)
    {


        LambdaQueryWrapper<TExpend> query = Wrappers.<TExpend>lambdaQuery();

        query.like(StrUtil.isNotBlank(param.getName()),TExpend::getName,param.getName());

        List<TExpend> list = tExpendService.list(query);
        ExcelUtil<TExpend> util = new ExcelUtil<TExpend>(TExpend.class);
        return util.exportExcel(list, "财务管理数据");
    }
}
