package com.danny.web.controller.biz;

import cn.hutool.core.util.StrUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.danny.common.annotation.Log;
import com.danny.common.core.domain.R;
import com.danny.common.utils.SecurityUtils;
import com.danny.common.utils.poi.ExcelUtil;
import com.danny.framework.config.ServerConfig;
import com.danny.system.common.ProjectType;
import com.danny.system.domain.TProject;
import com.danny.system.domain.TSafetyAssistance;
import com.danny.system.domain.TSafetyAssistanceRecords;
import com.danny.system.domain.TSafetyCheck;
import com.danny.system.service.TProjectService;
import com.danny.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.danny.common.enums.BusinessType;
import com.danny.system.service.TSafetyAssistanceRecordsService;
import com.danny.system.service.TSafetyAssistanceService;
import com.danny.system.service.TSafetyCheckService;
import org.springframework.web.bind.annotation.*;
import com.danny.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 安全检查类项目Controller
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/project")
public class TProjectController extends BaseController
{
    @Autowired
    private TProjectService tProjectService;

    @Autowired
    private TSafetyCheckService safetyCheckService;

    @Autowired
    private TSafetyAssistanceService safetyAssistanceService;

    @Autowired
    private TSafetyAssistanceRecordsService safetyAssistanceRecordsService;

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 查询安全检查类项目列表
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:list')")
    @GetMapping("/list")
    public R<Page> list(TProject param, HttpServletRequest request, HttpServletResponse response)
    {
        LambdaQueryWrapper<TProject> query = Wrappers.<TProject>lambdaQuery();
        query.like(StrUtil.isNotBlank(param.getProjectName()),TProject::getProjectName,param.getProjectName());
        query.eq(StrUtil.isNotBlank(param.getCategory()),TProject::getCategory,param.getCategory());
        query.like(StrUtil.isNotBlank(param.getManager()),TProject::getManager,param.getManager());
        query.eq(StrUtil.isNotBlank(param.getLocation()),TProject::getLocation,param.getLocation());
        PageDomain pageDomain = new PageDomain(request, response);
        Page<TProject> page = tProjectService.page(new Page(pageDomain.getPageNum(), pageDomain.getPageSize()), query);
        Set<String> projectIds = page.getRecords().stream().map(i -> i.getId()).collect(Collectors.toSet());
        if (param.getCategory() != null && projectIds.size() > 0) {
            ProjectType projectType = ProjectType.codeOf(param.getCategory());
            switch (projectType) {
                case CHECK:
                    List<TSafetyCheck> safetyChecks = safetyCheckService
                            .list(new LambdaQueryWrapper<TSafetyCheck>().in(TSafetyCheck::getProjectId, projectIds));
                    Map<String, TSafetyCheck> safetyCheckMap =
                            safetyChecks.stream().collect(Collectors.toMap(TSafetyCheck::getProjectId, Function.identity()));
                    page.getRecords().forEach(i -> i.setSafetyCheck(safetyCheckMap.get(i.getId())));
                    break;
                case ASSISTANCE:
                    List<TSafetyAssistance> safetyAssistance = safetyAssistanceService
                            .list(new LambdaQueryWrapper<TSafetyAssistance>().in(TSafetyAssistance::getProjectId, projectIds));
                    Map<String,TSafetyAssistance> safetyAssistanceMap =
                            safetyAssistance.stream().collect(Collectors.toMap(TSafetyAssistance::getProjectId, Function.identity()));
                    page.getRecords().forEach(i -> i.setSafetyAssistance(safetyAssistanceMap.get(i.getId())));
                    break;
            }
        }
        return R.data(page);
    }

    /**
     * 获取安全检查类项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:query')")
    @GetMapping(value = "/{id}")
    public R<TProject> detail(@PathVariable("id") String id)
    {
        TProject projectInfo = tProjectService.getById(id);
        ProjectType projectType = ProjectType.codeOf(projectInfo.getCategory());
        switch (projectType) {
            case ASSISTANCE:
                TSafetyAssistance safetyAssistance = safetyAssistanceService
                        .getOne(new LambdaQueryWrapper<TSafetyAssistance>().eq(TSafetyAssistance::getProjectId, projectInfo.getId()));
                if(safetyAssistance != null) {
                    projectInfo.setSafetyAssistance(safetyAssistance);
                } else {
                    safetyAssistance = new TSafetyAssistance();
                    safetyAssistance.setProjectId(id);
                    projectInfo.setSafetyAssistance(safetyAssistance);
                }
                List<TSafetyAssistanceRecords> safetyAssistanceRecords =
                        safetyAssistanceRecordsService.list(new LambdaQueryWrapper<TSafetyAssistanceRecords>().eq(TSafetyAssistanceRecords::getProjectId, id));
                safetyAssistanceRecords.forEach(i -> {
                    i.setUrl(serverConfig.getUrl() + i.getRelateFile());
                });
                projectInfo.getSafetyAssistance().setAssistanceRecords(safetyAssistanceRecords);

            case CHECK:
                TSafetyCheck safetyCheck = safetyCheckService
                        .getOne(new LambdaQueryWrapper<TSafetyCheck>().eq(TSafetyCheck::getProjectId, projectInfo.getId()));
                if (safetyCheck != null) {
                    projectInfo.setSafetyCheck(safetyCheck);
                } else {
                    safetyCheck = new TSafetyCheck();
                    safetyCheck.setProjectId(id);
                    projectInfo.setSafetyCheck(safetyCheck);
                }
                break;
        }
        return R.data(projectInfo);
    }

    /**
     * 新增安全检查类项目
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:add')")
    @Log(title = "安全检查类项目", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  TProject tProject)
    {
        if (StrUtil.isBlank(tProject.getProjectName())) {
            return R.fail("项目名称未指定");
        }
        TProject one = tProjectService.getOne(Wrappers.lambdaQuery(TProject.class)
                .eq(TProject::getProjectName, tProject.getProjectName()));
        if(one != null) {
            return R.fail("项目名称重复");
        }
        String optUser = SecurityUtils.getUsername();
        tProject.setCreateBy(optUser);
        Date optTime = new Date();
        tProject.setCreateTime(optTime);
        tProjectService.su(tProject);
        ProjectType projectType = ProjectType.codeOf(tProject.getCategory());
        switch (projectType) {
            case ASSISTANCE:
                TSafetyAssistance safetyAssistance = tProject.getSafetyAssistance();
                safetyAssistance.setProjectId(tProject.getId());
                safetyAssistance.setCreateBy(optUser);
                safetyAssistance.setCreateTime(optTime);
                safetyAssistanceService.su(safetyAssistance);
                List<TSafetyAssistanceRecords> assistanceRecords = safetyAssistance.getAssistanceRecords();
                if (assistanceRecords !=null && assistanceRecords.size() > 0) {
                    assistanceRecords.forEach(i -> {
                        i.setProjectId(tProject.getId());
                        i.setSafetyAssistanceId(safetyAssistance.getId());
                        i.setCreateBy(optUser);
                        i.setCreateTime(optTime);
                    });
                    safetyAssistanceRecordsService.saveBatch(assistanceRecords);
                }
                break;
            case CHECK:
                TSafetyCheck safetyCheck = tProject.getSafetyCheck();
                safetyCheck.setProjectId(tProject.getId());
                safetyCheck.setCreateBy(optUser);
                safetyCheck.setCreateTime(optTime);
                safetyCheckService.su(safetyCheck);
                break;
        }
        return R.data(tProject);
    }

    /**
     * 修改安全检查类项目
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:edit')")
    @Log(title = "安全检查类项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated TProject tProject)
    {
        logger.debug("更新项目信息，ID:{},name:{}", tProject.getId(), tProject.getProjectName());
        String optUser = SecurityUtils.getUsername();
        tProject.setUpdateBy(optUser);
        Date updateTime = new Date();
        tProject.setUpdateTime(updateTime);
        tProjectService.su(tProject);
        ProjectType projectType = ProjectType.codeOf(tProject.getCategory());
        switch (projectType) {
            case ASSISTANCE:
                TSafetyAssistance safetyAssistance = tProject.getSafetyAssistance();
                safetyAssistance.setProjectId(tProject.getId());
                if (safetyAssistance.getId() != null){
                    safetyAssistance.setUpdateBy(optUser);
                    safetyAssistance.setUpdateTime(updateTime);
                } else {
                    safetyAssistance.setCreateBy(optUser);
                    safetyAssistance.setCreateTime(updateTime);
                }
                safetyAssistanceService.su(safetyAssistance);
                safetyAssistanceRecordsService
                        .remove(new LambdaUpdateWrapper<TSafetyAssistanceRecords>()
                                .eq(TSafetyAssistanceRecords::getProjectId, tProject.getId()));
                if (safetyAssistance.getAssistanceRecords().size() > 0) {
                    safetyAssistanceRecordsService.saveBatch(safetyAssistance.getAssistanceRecords());
                }

            case CHECK:
                TSafetyCheck safetyCheck = tProject.getSafetyCheck();
                logger.debug("更新安全检查项目，{}", safetyCheck);
                safetyCheck.setProjectId(tProject.getId());
                if(safetyCheck.getId() != null){
                    safetyCheck.setUpdateBy(optUser);
                    safetyCheck.setUpdateTime(updateTime);
                } else {
                    safetyCheck.setCreateBy(optUser);
                    safetyCheck.setCreateTime(updateTime);
                }
                safetyCheckService.su(safetyCheck);
                break;
        }

        return R.data(tProject);
    }


    /**
     * 删除安全检查类项目
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:remove')")
    @Log(title = "安全检查类项目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R remove(@PathVariable String[] ids)
    {
        return R.status(tProjectService.deleteTProjectByIds(ids));
    }


    /**
     * 导出安全检查类项目列表
     */
    @PreAuthorize("@ss.hasPermi('project:tProject:export')")
    @Log(title = "安全检查类项目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(TProject param)
    {

        LambdaQueryWrapper<TProject> query = Wrappers.<TProject>lambdaQuery();
        query.like(StrUtil.isNotBlank(param.getProjectName()),TProject::getProjectName,param.getProjectName());
        query.eq(StrUtil.isNotBlank(param.getCategory()),TProject::getCategory,param.getCategory());
        query.like(StrUtil.isNotBlank(param.getManager()),TProject::getManager,param.getManager());
        query.eq(StrUtil.isNotBlank(param.getLocation()),TProject::getLocation,param.getLocation());

        List<TProject> list = tProjectService.list(query);
        ExcelUtil<TProject> util = new ExcelUtil<TProject>(TProject.class);
        return util.exportExcel(list, "安全检查类项目数据");
    }

}
