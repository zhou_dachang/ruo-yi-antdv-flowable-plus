package com.danny.web.controller.biz;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.danny.common.config.AiDexConfig;
import com.danny.common.constant.Constants;
import com.danny.common.core.controller.BaseController;
import com.danny.common.core.domain.AjaxResult;
import com.danny.common.utils.StringUtils;
import com.danny.common.utils.file.FileUploadUtils;
import com.danny.framework.config.ServerConfig;
import com.danny.system.common.FileType;
import com.danny.system.domain.TFileTags;
import com.danny.system.domain.vo.FileDataVo;
import com.danny.system.service.TFileTagsService;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/data")
public class DataController extends BaseController implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger log = LoggerFactory.getLogger(DataController.class);

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private TFileTagsService fileTagsService;

    // 刷新线程池 -> 如果数据都没了则启用后台线程进行刷新,让用户无感知 -> 核心线程数 1, 最大线程数 2
    ListeningExecutorService backgroundRefreshPools =
            MoreExecutors.listeningDecorator(new ThreadPoolExecutor(
                    1,
                    2,
                    0L,
                    TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>()));
    LoadingCache<String, String[]> cache;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        cache = CacheBuilder.newBuilder()
                // 缓存刷新时间
                .refreshAfterWrite(10, TimeUnit.MINUTES)
                // 设置缓存在写入invalidTime分钟后失效
                .expireAfterWrite(30, TimeUnit.MINUTES)
                // 设置缓存个数
                .maximumSize(100000)
                .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                .recordStats()
                .build(new CacheLoader<String, String[]>() {
                    // 当本地缓存命没有中时，调用load方法获取结果并将结果缓存
                    @Override
                    public String[] load(String appKey) {
                        return getFromDB(appKey);
                    }
                    @Override
                    public Map<String, String[]> loadAll(Iterable<? extends String> keys) throws Exception {
                        Map<String,String[]> all =
                                fileTagsService.list().stream().collect(Collectors.toMap(
                                        i -> i.getFilePath(),
                                        i-> i.getTags().split(",")
                                ));
                        System.out.println("加载文件标签缓存：" + all);
                        return all;
                    }
                    // 刷新时，开启一个新线程异步刷新，老请求直接返回旧值，防止耗时过长
                    @Override
                    public ListenableFuture<String[]> reload(String key, String[] oldValue) {
                        return backgroundRefreshPools.submit(() -> getFromDB(key));
                    }

                    // 数据库进行查询
                });

    }

    private String[] getFromDB (String key) {
        TFileTags one = fileTagsService.getOne(new LambdaQueryWrapper<TFileTags>().eq(TFileTags::getFilePath, key));
        if(one != null){
            return one.getTags().split(",");
        }
        return new String[]{};
    }

    /**
     * 获取当前的路径下的文件列表，包括文件夹
     * @return
     */
    @PreAuthorize("@ss.hasPermi('data:file:list')")
    @GetMapping("currentPathFiles")
    public AjaxResult currentPathFiles(String path) {
        File directory = new File(StringUtils.isEmpty(path) ? AiDexConfig.getFileStorePath()
                : (AiDexConfig.getFileStorePath() + path));
        log.debug("获取文件路径：{}", directory.getPath());
        if(directory.isDirectory()) {
            File[] files = directory.listFiles();
            List<FileDataVo> collect = Arrays.stream(files).filter(i -> !i.isHidden()).map(i -> {
                FileDataVo vo = new FileDataVo();
                vo.setName(i.getName());
                vo.setType(i.isDirectory() ? FileType.Directory.name() : FileType.File.name());
                vo.setFileSize(i.length());
                StringBuilder url = new StringBuilder(serverConfig.getUrl());
                url.append(Constants.RESOURCE_PREFIX);
                url.append(File.separator + "fileStore");
                url.append(path + i.getName());
                vo.setExtension(FilenameUtils.getExtension(i.getName()));
                vo.setUrl(url.toString());
                String[] tags;
                String key = directory.getPath() + File.separator + i.getName();
                try {
                    tags = cache.get(key);
                } catch (ExecutionException e) {
                    log.error(e.getMessage(), e);
                    tags = getFromDB(key);
                }
                vo.setTags(tags);
                return vo;
            }).collect(Collectors.toList());
            return AjaxResult.success(collect);
        } else {
            return AjaxResult.error("该路径不是有效文件路径或已不存在");
        }
    }
    @PreAuthorize("@ss.hasPermi('data:file:list')")
    @GetMapping("search")
    public AjaxResult search(String path, String keyword) {
        StringBuilder folderNameBuilder = new StringBuilder(AiDexConfig.getFileStorePath());
        if (path != null) {
            folderNameBuilder.append(path);
        }
        File folder = new File(folderNameBuilder.toString());
        if (folder.isFile() || !folder.exists()) {
            return AjaxResult.error("文件路径错误");
        }
        List<File> result = new ArrayList<>();
        searchFile(result, folder, keyword);
        List<FileDataVo> fileDataVoList = result.stream().map(i -> {
            FileDataVo vo = new FileDataVo();
            vo.setName(i.getName());
            vo.setType(i.isDirectory() ? FileType.Directory.name() : FileType.File.name());
            vo.setFileSize(i.length());
            vo.setExtension(FilenameUtils.getExtension(i.getName()));
            vo.setUrl(convertPathToUrl(i.getAbsolutePath()));
            //TODO

            return vo;
        }).collect(Collectors.toList());
        return AjaxResult.success(fileDataVoList);
    }

    public void searchFile(List<File> result, File directory, String targetFileName) {
        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            //
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        // 递归进入子目录
                        searchFile(result, file, targetFileName);
                    } else {
                        if (file.getName().equals(targetFileName)
                                || file.getName().indexOf(targetFileName) >= 0) {
                            result.add(file);
                        }
                        try {
                            System.out.println("搜索关键词：" + targetFileName);
                            String[] tags = cache.get(file.getAbsolutePath());
                            System.out.println("缓存key:" + file.getAbsolutePath() + ", 缓存:" + Arrays.asList(tags));
                            if(Arrays.stream(tags).anyMatch(i -> i.indexOf(targetFileName) >= 0)) {
                                result.add(file);
                            }
                        } catch (ExecutionException e) {
                            log.error(e.getMessage(),e);
                        }
                    }
                }
            }
        }
    }

    private String convertPathToUrl(String filePath) {
        StringBuilder url = new StringBuilder(serverConfig.getUrl());
        url.append(Constants.RESOURCE_PREFIX);
        url.append(File.separator + "fileStore");
        url.append(filePath.substring(AiDexConfig.getFileStorePath().length()));
        return url.toString();
    }

    /**
     * 创建文件夹
     * @param filePath
     * @return
     */
    @PreAuthorize("@ss.hasPermi('data:file:add')")
    @PutMapping("createFolder")
    public  AjaxResult createFolder(String filePath, String folderName) {
        System.out.println(filePath + ":" + folderName);
        StringBuilder folderNameBuilder = new StringBuilder(AiDexConfig.getFileStorePath());
        if (filePath == null) {
            folderNameBuilder.append(File.separator).append(folderName);
        } else {
            if(!filePath.endsWith("/")){
                folderNameBuilder.append(filePath + File.separator + folderName);
            } else {
                folderNameBuilder.append(filePath + folderName);
            }
        }
        File folder = new File(folderNameBuilder.toString());
        if(folder.mkdir()) {
            return AjaxResult.success("文件夹创建成功", filePath);
        }
        return AjaxResult.error("文件夹创建失败");
    }

    /**
     * 创建文件
     * @param filePath
     * @param fileName
     * @return
     */
    @PostMapping("createFile")
    public  AjaxResult createFile(String filePath, String fileName) {
        return AjaxResult.success();
    }

    /**
     *
     * @param originName  begin with '/'
     * @param newName
     * @return
     */
    @PreAuthorize("@ss.hasPermi('data:file:rename')")
    @PostMapping("renameFolder")
    public AjaxResult renameFolder(String originName, String newName) {
        System.out.println("originName: " + originName);
        System.out.println("newName: " + newName);
        if (StringUtils.isEmpty(originName)) {
            return AjaxResult.error("参数错误");
        }
        File folder = new File( AiDexConfig.getFileStorePath() + originName);
        if(folder.isDirectory() && folder.exists()) {
            File newFolder = new File(folder.getParent(), newName);
            if (folder.renameTo(newFolder)) {
                return AjaxResult.success("修改成功");
            } else {
                return AjaxResult.success("修改失败");
            }
        } else {
            return AjaxResult.error("文件夹不存在");
        }
    }
    @PreAuthorize("@ss.hasPermi('data:file:rename')")
    @PostMapping("renameFile")
    public AjaxResult renameFile(String filePath, String newName) {
        System.out.println("filePath: " + filePath);
        System.out.println("newName: " + newName);
        File oldFile = new File(AiDexConfig.getFileStorePath() + filePath);
        if(!oldFile.exists()){
            return AjaxResult.error("待修改文件已不存在");
        }
        if(!oldFile.isFile()){
            return AjaxResult.success("修改失败");
        }
        File newFile = new File(oldFile.getParent(), newName);
        return oldFile.renameTo(newFile)? AjaxResult.success("修改成功"): AjaxResult.success("修改失败");
    }


    /**
     * path不传当作根目录
     * @param path
     * @param fileNames
     * @return
     */
    @PreAuthorize("@ss.hasPermi('data:file:delete')")
    @DeleteMapping("deleteBatch")
    public AjaxResult deleteBatch(String path, String[] fileNames) {
        if(path == null) {
            path = File.separator;
        }
        File currentPath = new File(AiDexConfig.getFileStorePath() + path);
        if (!currentPath.exists() || !currentPath.isDirectory()){
            return AjaxResult.error("指定的目录不存在:" + currentPath.getAbsolutePath());
        }
        if (fileNames != null && fileNames.length > 0) {
            for (String fileName : fileNames) {
                try {
                    File targetFile = new File(currentPath, fileName);
                    if (targetFile.isFile()) {
                        targetFile.delete();
                    } else if (targetFile.isDirectory()) {
                        //
                        deleteFiles(targetFile.getAbsolutePath());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    // logit
                }
            }
        }
        return AjaxResult.success("删除成功");
    }
    @PreAuthorize("@ss.hasPermi('data:file:delete')")
    @DeleteMapping("deleteFile")
    public AjaxResult deleteFile(String filePath, String fileName) {
        System.out.println("删除文件：" + filePath + ":" + fileName);
        File file = new File(AiDexConfig.getFileStorePath() + filePath + fileName);
        //TODO 删除相应的标签
        if(file.exists() && file.isFile()) {
            if (file.delete()) {
                return AjaxResult.success("删除成功");
            } else {
                return AjaxResult.error("删除失败");
            }
        } else {
            return AjaxResult.error("删除失败");
        }
    }
    @PreAuthorize("@ss.hasPermi('data:file:delete')")
    @DeleteMapping("deleteFolder")
    public  AjaxResult deleteFolder(String folderPath) {
        System.out.println("删除文件夹：" + folderPath);
        File file = new File(AiDexConfig.getFileStorePath() + folderPath);
        if(file.exists() && file.isDirectory()) {
            deleteFiles(AiDexConfig.getFileStorePath() + folderPath);
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除失败");
    }

    @PostMapping("updateFileTags")
    public AjaxResult updateFileTags(String filePath, String[] tags) {
        File file = new File(AiDexConfig.getFileStorePath() + filePath);
        System.out.println("更新文件标签参数filePath:" + filePath);
        String filePathDb = AiDexConfig.getFileStorePath() + filePath;
        if(file.exists() && file.isFile()) {
            TFileTags fileTags =
                    fileTagsService.getOne(new LambdaQueryWrapper<TFileTags>().eq(TFileTags::getFilePath, filePathDb));
            if (fileTags != null) {
                fileTags.setTags(StringUtils.join(tags, ","));
                fileTagsService.saveOrUpdate(fileTags);
                cache.put(filePathDb, fileTags.getTags().split(","));
            }
        } else {
            return AjaxResult.error("修改的文件不存在");
        }
        return AjaxResult.success();
    }

    private void deleteFiles(String filePath)
    {
        File file = new File(filePath);
        if (file.isFile())  //判断是否为文件，是，则删除
        {
            file.delete();
        } else {
            String[] childFilePath = file.list();//获取文件夹下所有文件相对路径
            for (String path:childFilePath)
            {
                deleteFiles(file.getAbsoluteFile()+"/"+path);//递归，对每个都进行判断
            }
            file.delete();
        }
    }

    @PreAuthorize("@ss.hasPermi('data:file:add')")
    @PostMapping("upload")
    public AjaxResult upload(MultipartFile file, String path, String[] tags)
    {
        try {
            // 上传文件路径
            String originalFileName = file.getOriginalFilename();
            int fileNameLength = originalFileName.length();
            if (fileNameLength > FileUploadUtils.DEFAULT_FILE_NAME_LENGTH) {
                return AjaxResult.error("文件名超出长度限制。");
            }
            String desc;
            if (StringUtils.isEmpty(path) || path.equals("/")) {
                desc = AiDexConfig.getFileStorePath() + File.separator + originalFileName;
            } else {
                desc = AiDexConfig.getFileStorePath() + path + File.separator + originalFileName;
            }
            File descFile = new File(desc);
            file.transferTo(descFile);
            String url = serverConfig.getUrl() + AiDexConfig.getFileStorePath() + path  + originalFileName;
            // 20240218
            TFileTags fileTags = new TFileTags();
            fileTags.setFilePath(desc);
            fileTags.setTags(StringUtils.join(tags, ","));
            fileTagsService.saveOrUpdate(fileTags,
                    new LambdaQueryWrapper<TFileTags>().eq(TFileTags::getFilePath, desc));
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", originalFileName);
            ajax.put("url", url);
            return ajax;
        } catch (Exception e) {
            log.error("文件上传错误，", e);
            return AjaxResult.error(e.getMessage());
        }
    }


}
