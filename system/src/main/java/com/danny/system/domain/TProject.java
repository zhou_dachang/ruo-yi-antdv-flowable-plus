package com.danny.system.domain;

import java.math.BigDecimal;
import javax.swing.text.html.parser.Entity;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.danny.common.annotation.Excel;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.danny.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 安全检查类项目对象 t_project
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_project")
public class TProject extends BasePlusEntity<TProject>
{
    private static final long serialVersionUID = 1L;

//    public TProject(TProject project) {
//        BeanCopyUtils.copyProperties(project, this);
//    }

    /** 项目名 */
    @Excel(name = "项目名")
    @NotBlank(message = "项目名不允许为空")
    @TableField("project_name")
    private String projectName;

    /** 项目分类 */
    @Excel(name = "项目分类", dictType = "project_category")
    @NotBlank(message = "项目分类不允许为空")
    @TableField("category")
    private String category;

    /** 起始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("begin_date")
    private Date beginDate;

    /** 起始时间开始 */
    @TableField(exist = false)
    private String beginBeginDate;

    /** 起始时间结束 */
    @TableField(exist = false)
    private String endBeginDate;
    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("finish_date")
    private Date finishDate;

    /** 结束时间开始 */
    @TableField(exist = false)
    private String beginFinishDate;

    /** 结束时间结束 */
    @TableField(exist = false)
    private String endFinishDate;
    /** 项目经理 */
    @Excel(name = "项目经理")
    @TableField("manager")
    private String manager;

    /** 项目属地 */
    @Excel(name = "项目属地")
    @TableField("location")
    private String location;

    /** 进度 */
    @Excel(name = "进度")
    @TableField("schedule")
    private Integer schedule;

    @TableField("contract")
    private String contract;

    @TableField("contract_filename")
    private String contractFilename;

    /** 合同金额 */
    @TableField("contract_amount")
    private BigDecimal contractAmount;

    /** 应收预付款 */
    @TableField("advance_payment")
    private BigDecimal advancePayment;

    /** 实收预付款 */
    @TableField("paid_advance_payment")
    private BigDecimal paidAdvancePayment;

    /** 应收尾款 */
    @TableField("balance_payment")
    private BigDecimal balancePayment;

    /** 已收应收尾款 */
    @TableField("paid_balance_payment")
    private BigDecimal paidBalancePayment;

    /** 商务费用 */
    @TableField("business_expenses")
    private BigDecimal businessExpenses;

    /** 项目费用 */
    @TableField("program_expense")
    private BigDecimal programExpense;

    /** 客户联系人 */
    @TableField("customer_contact")
    private String customerContact;

    /** 客户联系方式 */
    @TableField("customer_contact_info")
    private String customerContactInfo;

    /** 备注 */
    @TableField("notes")
    private String notes;

    @TableField(exist = false)
    private TSafetyCheck safetyCheck = new TSafetyCheck();

    @TableField(exist = false)
    private TSafetyAssistance safetyAssistance = new TSafetyAssistance();

    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }

    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }

    public void setBeginDate(Date beginDate) 
    {
        this.beginDate = beginDate;
    }

    public Date getBeginDate() 
    {
        return beginDate;
    }

    public void setFinishDate(Date finishDate) 
    {
        this.finishDate = finishDate;
    }

    public Date getFinishDate() 
    {
        return finishDate;
    }

    public void setManager(String manager) 
    {
        this.manager = manager;
    }

    public String getManager() 
    {
        return manager;
    }

    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }

    public void setSchedule(Integer schedule) 
    {
        this.schedule = schedule;
    }

    public Integer getSchedule() 
    {
        return schedule;
    }

    public void setContractAmount(BigDecimal contractAmount) 
    {
        this.contractAmount = contractAmount;
    }

    public BigDecimal getContractAmount() 
    {
        return contractAmount;
    }

    public void setAdvancePayment(BigDecimal advancePayment) 
    {
        this.advancePayment = advancePayment;
    }

    public BigDecimal getAdvancePayment() 
    {
        return advancePayment;
    }

    public void setPaidAdvancePayment(BigDecimal paidAdvancePayment) 
    {
        this.paidAdvancePayment = paidAdvancePayment;
    }

    public BigDecimal getPaidAdvancePayment() 
    {
        return paidAdvancePayment;
    }

    public void setBalancePayment(BigDecimal balancePayment) 
    {
        this.balancePayment = balancePayment;
    }

    public BigDecimal getBalancePayment() 
    {
        return balancePayment;
    }

    public void setPaidBalancePayment(BigDecimal paidBalancePayment) 
    {
        this.paidBalancePayment = paidBalancePayment;
    }

    public BigDecimal getPaidBalancePayment() 
    {
        return paidBalancePayment;
    }

    public void setBusinessExpenses(BigDecimal businessExpenses) 
    {
        this.businessExpenses = businessExpenses;
    }

    public BigDecimal getBusinessExpenses() 
    {
        return businessExpenses;
    }

    public void setProgramExpense(BigDecimal programExpense) 
    {
        this.programExpense = programExpense;
    }

    public BigDecimal getProgramExpense() 
    {
        return programExpense;
    }

    public void setCustomerContact(String customerContact) 
    {
        this.customerContact = customerContact;
    }

    public String getCustomerContact() 
    {
        return customerContact;
    }

    public void setCustomerContactInfo(String customerContactInfo) 
    {
        this.customerContactInfo = customerContactInfo;
    }

    public String getCustomerContactInfo() 
    {
        return customerContactInfo;
    }

    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("projectName", getProjectName())
            .append("category", getCategory())
            .append("beginDate", getBeginDate())
            .append("finishDate", getFinishDate())
            .append("manager", getManager())
            .append("location", getLocation())
            .append("schedule", getSchedule())
            .append("contractAmount", getContractAmount())
            .append("advancePayment", getAdvancePayment())
            .append("paidAdvancePayment", getPaidAdvancePayment())
            .append("balancePayment", getBalancePayment())
            .append("paidBalancePayment", getPaidBalancePayment())
            .append("businessExpenses", getBusinessExpenses())
            .append("programExpense", getProgramExpense())
            .append("customerContact", getCustomerContact())
            .append("customerContactInfo", getCustomerContactInfo())
            .append("notes", getNotes())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
