package com.danny.system.domain;
import lombok.Data;
import com.danny.common.utils.log.annotation.FieldRemark;
import com.danny.common.utils.log.annotation.LogField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.danny.common.annotation.Excel;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.danny.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_safety_assistance")
public class TSafetyAssistance extends BasePlusEntity<TSafetyAssistance>{

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "帮扶频次")
    @TableField("project_id")
    private String projectId;

    /** 帮扶频次 */
    @Excel(name = "帮扶频次")
    @TableField("frequency")
    private Long frequency;

    @Excel(name = "已完成次数")
    @TableField("finished_count")
    private Long finishedCount;

    @TableField(exist = false)
    private List<TSafetyAssistanceRecords> assistanceRecords = new ArrayList<>();

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setFrequency(Long frequency)
    {
        this.frequency = frequency;
    }

    public Long getFrequency()
    {
        return frequency;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("projectId", getProjectId())
                .append("frequency", getFrequency())
                .append("finishedCount", getFinishedCount())
                .append("remark", getRemark())
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createDept", getCreateDept())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("updateIp", getUpdateIp())
                .append("version", getVersion())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
