package com.danny.system.domain.vo;

import lombok.Data;

@Data
public class FileDataVo {
    //文件名
    private String name;
    //file directory
    private String type;
    //体积    字节
    private Long fileSize;
    // file类型下的文件类型
    private String extension;
    // 文件路径
    private String url;

    private String[] tags;
}
