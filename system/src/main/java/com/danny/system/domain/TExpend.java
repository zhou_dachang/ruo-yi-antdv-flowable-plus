package com.danny.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import com.danny.common.annotation.Excel;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.danny.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_expend")
public class TExpend extends BasePlusEntity<TExpend>{
    private static final long serialVersionUID = 1L;

    /** 支出名目 */
    @Excel(name = "支出名目")
    @NotBlank(message = "支出名目不允许为空")
    @TableField("name")
    private String name;

    /** 费用 */
    @Excel(name = "费用")
    @TableField("expenses")
    private BigDecimal expenses;

    /** 说明 */
    @TableField("notes")
    private String notes;

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setExpenses(BigDecimal expenses)
    {
        this.expenses = expenses;
    }

    public BigDecimal getExpenses()
    {
        return expenses;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public String getNotes()
    {
        return notes;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("name", getName())
                .append("expenses", getExpenses())
                .append("notes", getNotes())
                .append("remark", getRemark())
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createDept", getCreateDept())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("updateIp", getUpdateIp())
                .append("version", getVersion())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
