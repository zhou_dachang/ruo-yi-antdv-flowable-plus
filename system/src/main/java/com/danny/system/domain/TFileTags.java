package com.danny.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.danny.common.annotation.Excel;
import com.danny.common.core.domain.BasePlusEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * FileTags对象 t_file_tags
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2024-02-18
 */
@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_file_tags")
public class TFileTags extends BasePlusEntity<TFileTags>
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("file_path")
    private String filePath;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("tags")
    private String tags;

    public void setFilePath(String filePath) 
    {
        this.filePath = filePath;
    }

    public String getFilePath() 
    {
        return filePath;
    }

    public void setTags(String tags) 
    {
        this.tags = tags;
    }

    public String getTags() 
    {
        return tags;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("filePath", getFilePath())
            .append("tags", getTags())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
