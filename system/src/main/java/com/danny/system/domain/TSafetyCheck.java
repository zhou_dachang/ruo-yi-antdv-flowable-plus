package com.danny.system.domain;

import lombok.Data;
import com.danny.common.utils.log.annotation.FieldRemark;
import com.danny.common.utils.log.annotation.LogField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.danny.common.annotation.Excel;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.danny.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * safetyCheck对象 t_safety_check
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_safety_check")
public class TSafetyCheck extends BasePlusEntity<TSafetyCheck>
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("check_amount")
    private Long checkAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("project_id")
    private String projectId;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("finished_check_amount")
    private Long finishedCheckAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}")
    @TableField("reexamine_amount")
    private Long reexamineAmount;

    public void setCheckAmount(Long checkAmount) 
    {
        this.checkAmount = checkAmount;
    }

    public Long getCheckAmount() 
    {
        return checkAmount;
    }

    public void setProjectId(String projectId) 
    {
        this.projectId = projectId;
    }

    public String getProjectId() 
    {
        return projectId;
    }

    public void setFinishedCheckAmount(Long finishedCheckAmount) 
    {
        this.finishedCheckAmount = finishedCheckAmount;
    }

    public Long getFinishedCheckAmount() 
    {
        return finishedCheckAmount;
    }

    public void setReexamineAmount(Long reexamineAmount) 
    {
        this.reexamineAmount = reexamineAmount;
    }

    public Long getReexamineAmount() 
    {
        return reexamineAmount;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("checkAmount", getCheckAmount())
            .append("projectId", getProjectId())
            .append("finishedCheckAmount", getFinishedCheckAmount())
            .append("reexamineAmount", getReexamineAmount())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
