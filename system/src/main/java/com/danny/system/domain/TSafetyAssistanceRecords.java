package com.danny.system.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import com.danny.common.utils.log.annotation.FieldRemark;
import com.danny.common.utils.log.annotation.LogField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.danny.common.annotation.Excel;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.danny.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
@Getter
@Setter
@Accessors(chain = true)
@TableName("t_safety_assistance_records")
public class TSafetyAssistanceRecords extends BasePlusEntity<TSafetyAssistanceRecords> {
    private static final long serialVersionUID = 1L;

    /** 项目ID */
    @Excel(name = "项目ID")
    @NotBlank(message = "项目ID不允许为空")
    @TableField("project_id")
    private String projectId;

    /** 安全帮扶id */
    @Excel(name = "安全帮扶id")
    @NotBlank(message = "安全帮扶id不允许为空")
    @TableField("safety_assistance_id")
    private String safetyAssistanceId;

    @Excel(name = "附件名称")
    @TableField("name")
    private String name;

    /** 相关附件 */
    @Excel(name = "相关附件")
    @TableField("relate_file")
    private String relateFile;

    @TableField(exist = false)
    private String url;

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setSafetyAssistanceId(String safetyAssistanceId)
    {
        this.safetyAssistanceId = safetyAssistanceId;
    }

    public String getSafetyAssistanceId()
    {
        return safetyAssistanceId;
    }

    public void setRelateFile(String relateFile)
    {
        this.relateFile = relateFile;
    }

    public String getRelateFile()
    {
        return relateFile;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("projectId", getProjectId())
                .append("safetyAssistanceId", getSafetyAssistanceId())
                .append("relateFile", getRelateFile())
                .append("remark", getRemark())
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createDept", getCreateDept())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("updateIp", getUpdateIp())
                .append("version", getVersion())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
