package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TSafetyAssistance;
import org.springframework.transaction.annotation.Transactional;

public interface TSafetyAssistanceService extends IService<TSafetyAssistance>{
    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TSafetyAssistance entity);



    /**
     * 批量删除安全帮扶
     * @param ids 需要删除的安全帮扶ID集合
     * @return 结果
     */
    public boolean deleteTSafetyAssistanceByIds(String[] ids);
}
