package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TFileTags;
import org.springframework.transaction.annotation.Transactional;

/**
 * FileTagsService接口
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2024-02-18
 */
public interface TFileTagsService extends IService<TFileTags>
{
    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TFileTags entity);



    /**
     * 批量删除FileTags
     * @param ids 需要删除的FileTagsID集合
     * @return 结果
     */
    public boolean deleteTFileTagsByIds(String[] ids);


}
