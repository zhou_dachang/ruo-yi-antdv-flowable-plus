package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TProject;
import org.springframework.transaction.annotation.Transactional;
/**
 * 安全检查类项目Service接口
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
public interface TProjectService extends IService<TProject>
{
    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TProject entity);



    /**
     * 批量删除安全检查类项目
     * @param ids 需要删除的安全检查类项目ID集合
     * @return 结果
     */
    public boolean deleteTProjectByIds(String[] ids);


}
