package com.danny.system.service.impl;


import java.util.Arrays;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.danny.system.domain.TProject;
import com.danny.system.mapper.TProjectMapper;
import com.danny.system.service.TProjectService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * 安全检查类项目Service业务层处理
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
@Service
@Transactional(readOnly = true)
public class TProjectServiceImpl extends ServiceImpl<TProjectMapper, TProject> implements TProjectService
{
    private static final Logger log = LoggerFactory.getLogger(TProjectServiceImpl.class);

    /**
     * 保存安全检查类项目
     * @param tProject
     * @return 结果
     */
    @Override
    public boolean su(TProject tProject)
    {
        return super.saveOrUpdate(tProject);
    }


    /**
     * 批量删除安全检查类项目
     * @param ids 需要删除的安全检查类项目ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTProjectByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }

}
