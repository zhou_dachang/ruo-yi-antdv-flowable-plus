package com.danny.system.service;

import com.danny.system.domain.WarmingRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 预警规则 服务类
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
public interface IWarmingRuleService extends IService<WarmingRule> {

}
