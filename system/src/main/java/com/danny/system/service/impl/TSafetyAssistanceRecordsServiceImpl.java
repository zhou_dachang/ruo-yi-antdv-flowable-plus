package com.danny.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Arrays;
import java.util.List;

import com.danny.system.domain.TSafetyAssistanceRecords;
import com.danny.system.mapper.TSafetyAssistanceRecordsMapper;
import com.danny.system.service.TSafetyAssistanceRecordsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TSafetyAssistanceRecordsServiceImpl
        extends ServiceImpl<TSafetyAssistanceRecordsMapper, TSafetyAssistanceRecords>
        implements TSafetyAssistanceRecordsService {
    private static final Logger log = LoggerFactory.getLogger(TSafetyAssistanceRecordsServiceImpl.class);

    /**
     * 保存安全帮扶记录
     * @param tSafetyAssistanceRecords
     * @return 结果
     */
    @Override
    public boolean su(TSafetyAssistanceRecords tSafetyAssistanceRecords)
    {
        return super.saveOrUpdate(tSafetyAssistanceRecords);
    }


    /**
     * 批量删除安全帮扶记录
     * @param ids 需要删除的安全帮扶记录ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTSafetyAssistanceRecordsByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }

}
