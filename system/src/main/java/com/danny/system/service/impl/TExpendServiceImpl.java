package com.danny.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.danny.system.domain.TExpend;
import com.danny.system.mapper.TExpendMapper;
import com.danny.system.service.TExpendService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

@Service
@Transactional(readOnly = true)
public class TExpendServiceImpl extends ServiceImpl<TExpendMapper, TExpend> implements TExpendService {
    private static final Logger log = LoggerFactory.getLogger(TExpendServiceImpl.class);

    /**
     * 保存财务管理
     * @param tExpend
     * @return 结果
     */
    @Override
    public boolean su(TExpend tExpend)
    {
        return super.saveOrUpdate(tExpend);
    }


    /**
     * 批量删除财务管理
     * @param ids 需要删除的财务管理ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTExpendByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }
}
