package com.danny.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Arrays;

import com.danny.system.domain.TSafetyCheck;
import com.danny.system.mapper.TSafetyCheckMapper;
import com.danny.system.service.TSafetyCheckService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * safetyCheckService业务层处理
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
@Service
@Transactional(readOnly = true)
public class TSafetyCheckServiceImpl extends ServiceImpl<TSafetyCheckMapper, TSafetyCheck> implements TSafetyCheckService
{
    private static final Logger log = LoggerFactory.getLogger(TSafetyCheckServiceImpl.class);

    /**
     * 保存safetyCheck
     * @param tSafetyCheck
     * @return 结果
     */
    @Override
    public boolean su(TSafetyCheck tSafetyCheck)
    {
        return super.saveOrUpdate(tSafetyCheck);
    }


    /**
     * 批量删除safetyCheck
     * @param ids 需要删除的safetyCheckID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTSafetyCheckByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }

}
