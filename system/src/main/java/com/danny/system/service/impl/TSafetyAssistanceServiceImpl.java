package com.danny.system.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.Arrays;

import com.danny.system.domain.TSafetyAssistance;
import com.danny.system.mapper.TSafetyAssistanceMapper;
import com.danny.system.service.TSafetyAssistanceService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * 安全帮扶Service业务层处理
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-09
 */
@Service
@Transactional(readOnly = true)
public class TSafetyAssistanceServiceImpl extends ServiceImpl<TSafetyAssistanceMapper, TSafetyAssistance>
        implements TSafetyAssistanceService {
    private static final Logger log = LoggerFactory.getLogger(TSafetyAssistanceServiceImpl.class);

    /**
     * 保存安全帮扶
     * @param tSafetyAssistance
     * @return 结果
     */
    @Override
    public boolean su(TSafetyAssistance tSafetyAssistance)
    {
        return super.saveOrUpdate(tSafetyAssistance);
    }


    /**
     * 批量删除安全帮扶
     * @param ids 需要删除的安全帮扶ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTSafetyAssistanceByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }
}
