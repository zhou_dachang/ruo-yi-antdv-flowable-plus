package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TSafetyCheck;
import org.springframework.transaction.annotation.Transactional;
/**
 * safetyCheckService接口
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
public interface TSafetyCheckService extends IService<TSafetyCheck>
{
    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TSafetyCheck entity);



    /**
     * 批量删除safetyCheck
     * @param ids 需要删除的safetyCheckID集合
     * @return 结果
     */
    public boolean deleteTSafetyCheckByIds(String[] ids);


}
