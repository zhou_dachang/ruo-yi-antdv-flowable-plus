package com.danny.system.service.impl;

import com.danny.system.domain.WarmingGroup;
import com.danny.system.mapper.WarmingGroupMapper;
import com.danny.system.service.IWarmingGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预警组 服务实现类
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
@Service
public class WarmingGroupServiceImpl extends ServiceImpl<WarmingGroupMapper, WarmingGroup> implements IWarmingGroupService {

}
