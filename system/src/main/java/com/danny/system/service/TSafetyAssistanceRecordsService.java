package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TSafetyAssistanceRecords;
import org.springframework.transaction.annotation.Transactional;

public interface TSafetyAssistanceRecordsService extends IService<TSafetyAssistanceRecords> {
    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TSafetyAssistanceRecords entity);



    /**
     * 批量删除安全帮扶记录
     * @param ids 需要删除的安全帮扶记录ID集合
     * @return 结果
     */
    public boolean deleteTSafetyAssistanceRecordsByIds(String[] ids);
}
