package com.danny.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.danny.system.domain.TFileTags;
import com.danny.system.mapper.TFileTagsMapper;
import com.danny.system.service.TFileTagsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * FileTagsService业务层处理
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2024-02-18
 */
@Service
@Transactional(readOnly = true)
public class TFileTagsServiceImpl extends ServiceImpl<TFileTagsMapper, TFileTags> implements TFileTagsService
{
    private static final Logger log = LoggerFactory.getLogger(TFileTagsServiceImpl.class);

    /**
     * 保存FileTags
     * @param tFileTags
     * @return 结果
     */
    @Override
    public boolean su(TFileTags tFileTags)
    {
        return super.saveOrUpdate(tFileTags);
    }


    /**
     * 批量删除FileTags
     * @param ids 需要删除的FileTagsID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public boolean deleteTFileTagsByIds(String[] ids)
    {
        return super.removeByIds(Arrays.asList(ids));
    }

}
