package com.danny.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.danny.system.domain.TExpend;
import org.springframework.transaction.annotation.Transactional;

public interface TExpendService extends IService<TExpend> {

    /**
     * 保存
     * @param entity
     * @return boolean
     */
    @Transactional(readOnly = false)
    public boolean su(TExpend entity);



    /**
     * 批量删除财务管理
     * @param ids 需要删除的财务管理ID集合
     * @return 结果
     */
    public boolean deleteTExpendByIds(String[] ids);

}
