package com.danny.system.service.impl;

import com.danny.system.domain.WarmingRule;
import com.danny.system.mapper.WarmingRuleMapper;
import com.danny.system.service.IWarmingRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预警规则 服务实现类
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
@Service
public class WarmingRuleServiceImpl extends ServiceImpl<WarmingRuleMapper, WarmingRule> implements IWarmingRuleService {

}
