package com.danny.system.service;

import com.danny.system.domain.WarmingGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 预警组 服务类
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
public interface IWarmingGroupService extends IService<WarmingGroup> {

}
