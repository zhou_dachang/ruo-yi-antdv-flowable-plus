package com.danny.system.common;

public enum ProjectType {

    DESIGN("1"), CONSULTING("2"),ASSISTANCE("3"), CHECK("4"), REST("9");
    private ProjectType(String code) {
        this.code = code;
    }

    private String code;

    public String getCode() {
        return code;
    }

    public static ProjectType codeOf(String code) {
        switch (code) {
            case "1":
                return DESIGN;
            case "2":
                return CONSULTING;
            case "3":
                return ASSISTANCE;
            case "4":
                return CHECK;
            default:
                return REST;
        }
    }
}
