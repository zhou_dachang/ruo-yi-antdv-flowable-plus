package com.danny.system.mapper;

import com.danny.system.domain.WarmingRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 预警规则 Mapper 接口
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
public interface WarmingRuleMapper extends BaseMapper<WarmingRule> {

}
