package com.danny.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danny.system.domain.TSafetyAssistanceRecords;

import java.util.List;

public interface TSafetyAssistanceRecordsMapper extends BaseMapper<TSafetyAssistanceRecords>{

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TSafetyAssistanceRecords> findArr(TSafetyAssistanceRecords entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TSafetyAssistanceRecords> findListWithUnique(TSafetyAssistanceRecords entity);


    /**
     * 插入数据
     * @param entity
     * @return
     */
    public int add(TSafetyAssistanceRecords entity);

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public int edit(TSafetyAssistanceRecords entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     * @param entity
     * @return
     */
    public int rem(TSafetyAssistanceRecords entity);



    /**
     * 批量删除安全帮扶记录
     * @param ids 需要删除的安全帮扶记录ID集合
     * @return
     */
    public int deleteTSafetyAssistanceRecordsByIds(String[] ids);
}
