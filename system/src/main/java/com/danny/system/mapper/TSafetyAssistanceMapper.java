package com.danny.system.mapper;

import com.danny.system.domain.TSafetyAssistance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

public interface TSafetyAssistanceMapper extends BaseMapper<TSafetyAssistance>{
    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TSafetyAssistance> findArr(TSafetyAssistance entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TSafetyAssistance> findListWithUnique(TSafetyAssistance entity);


    /**
     * 插入数据
     * @param entity
     * @return
     */
    public int add(TSafetyAssistance entity);

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public int edit(TSafetyAssistance entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     * @param entity
     * @return
     */
    public int rem(TSafetyAssistance entity);



    /**
     * 批量删除安全帮扶
     * @param ids 需要删除的安全帮扶ID集合
     * @return
     */
    public int deleteTSafetyAssistanceByIds(String[] ids);
}
