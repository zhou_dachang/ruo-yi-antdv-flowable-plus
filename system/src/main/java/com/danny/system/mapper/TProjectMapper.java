package com.danny.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danny.system.domain.TProject;

import java.util.List;
/**
 * 安全检查类项目Mapper接口
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2023-12-04
 */
public interface TProjectMapper extends BaseMapper<TProject>
{


    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TProject> findArr(TProject entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TProject> findListWithUnique(TProject entity);


    /**
     * 插入数据
     * @param entity
     * @return
     */
    public int add(TProject entity);

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public int edit(TProject entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     * @param entity
     * @return
     */
    public int rem(TProject entity);



    /**
     * 批量删除安全检查类项目
     * @param ids 需要删除的安全检查类项目ID集合
     * @return
     */
    public int deleteTProjectByIds(String[] ids);


}
