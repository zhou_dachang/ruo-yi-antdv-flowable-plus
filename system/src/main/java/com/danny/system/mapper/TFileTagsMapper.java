package com.danny.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danny.system.domain.TFileTags;

import java.util.List;

/**
 * FileTagsMapper接口
 * @author zhouxuan
 * @email zhouxuan614@gmail.com
 * @date 2024-02-18
 */
public interface TFileTagsMapper extends BaseMapper<TFileTags>
{


    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TFileTags> findArr(TFileTags entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TFileTags> findListWithUnique(TFileTags entity);


    /**
     * 插入数据
     * @param entity
     * @return
     */
    public int add(TFileTags entity);

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public int edit(TFileTags entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     * @param entity
     * @return
     */
    public int rem(TFileTags entity);



    /**
     * 批量删除FileTags
     * @param ids 需要删除的FileTagsID集合
     * @return
     */
    public int deleteTFileTagsByIds(String[] ids);


}
