package com.danny.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danny.system.domain.TSafetyCheck;

public interface TSafetyCheckMapper extends BaseMapper<TSafetyCheck> {

}
