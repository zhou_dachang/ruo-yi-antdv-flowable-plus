package com.danny.system.mapper;

import com.danny.system.domain.WarmingGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 预警组 Mapper 接口
 * </p>
 *
 * @author danny
 * @since 2021-11-22
 */
public interface WarmingGroupMapper extends BaseMapper<WarmingGroup> {

}
