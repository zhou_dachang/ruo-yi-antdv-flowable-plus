package com.danny.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danny.system.domain.TExpend;
import java.util.List;

public interface TExpendMapper extends BaseMapper<TExpend> {
    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TExpend> findArr(TExpend entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象
     * @param entity
     * @return
     */
    public List<TExpend> findListWithUnique(TExpend entity);


    /**
     * 插入数据
     * @param entity
     * @return
     */
    public int add(TExpend entity);

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public int edit(TExpend entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     * @param entity
     * @return
     */
    public int rem(TExpend entity);



    /**
     * 批量删除财务管理
     * @param ids 需要删除的财务管理ID集合
     * @return
     */
    public int deleteTExpendByIds(String[] ids);
}
